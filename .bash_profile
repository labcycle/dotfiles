#
# ~/.bash_profile
#

export EDITOR=nvim
export XDG_CONFIG_HOME=~/.config
export XDG_DATA_HOME=~/.local/share
export XDG_CACHE_HOME=~/.cache

[[ -f ~/.bashrc ]] && . ~/.bashrc
