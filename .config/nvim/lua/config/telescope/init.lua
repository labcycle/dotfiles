local trouble = require('trouble.providers.telescope')

require('telescope').setup {
	defaults = {
		mappings = {
			i = { ['<c-t>'] = trouble.open_with_trouble },
			n = { ['<c-t>'] = trouble.open_with_trouble },
		},
	},
}

local map = vim.keymap
local builtin = require('telescope.builtin')

map.set('n', '<leader>ts', '<cmd>Telescope<cr>', { desc = 'open telescope' })
map.set('n', '<leader>ff', builtin.find_files, { desc = 'open telescope find files' })
map.set('n', '<leader>fg', builtin.live_grep, { desc = 'open telescope live grep' })
map.set('n', '<leader>fb', builtin.buffers, { desc = 'open telescope list buffers' })
map.set('n', '<leader>fh', builtin.help_tags, { desc = 'open telescope help tags' })
