require('Navigator').setup {}

local map  = vim.keymap

map.set({'n', 't'}, '<A-h>', '<CMD>NavigatorLeft<CR>')
map.set({'n', 't'}, '<A-l>', '<CMD>NavigatorRight<CR>')
map.set({'n', 't'}, '<A-k>', '<CMD>NavigatorUp<CR>')
map.set({'n', 't'}, '<A-j>', '<CMD>NavigatorDown<CR>')
map.set({'n', 't'}, '<A-p>', '<CMD>NavigatorPrevious<CR>')
