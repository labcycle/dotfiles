vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

require('nvim-tree').setup {}

local map = vim.keymap
local tree = require('nvim-tree.api').tree
local toggle = function()
	tree.toggle{find_file = true}
end

map.set('n', '<leader>v', toggle, { desc = 'toggle nvim-tree sidebar' })
