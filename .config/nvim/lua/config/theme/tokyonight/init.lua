require('tokyonight').setup {
	style = 'night',
	dim_inactive = true,
	terminal_colors = true,
}
