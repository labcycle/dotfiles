local treesitter_config = require('nvim-treesitter.configs')

treesitter_config.setup {
	highlight = {
		enable = true,
	},
	ensure_installed = { 'c', 'go', 'lua', 'query', 'rust', 'toml', 'ron' },
	auto_install = false,
	ignore_install = { 'javascript' },
	indent = { enable = true },
	incremental_selection = {
		enable = true,
		keymaps = {
			init_selection = 'gnn',
			node_incremental = 'grn',
			scope_incremental = 'grc',
			node_decremental = 'grm',
		},
	},
	refactor = {
		highlight_definitions = {
			enable = true,
			clear_on_curson_move = true,
		},
		highlight_current_scope = { enable = false },
		smart_rename = {
			enable = true,
			keymaps = {
				smart_rename = 'grr',
			},
		},
		navigation = {
			enable = true,
			keymaps = {
				goto_definition = 'gnd',
				list_definitions = 'gnD',
				list_definitions_toc = 'gn0',
				goto_next_usage = 'gnf',
				goto_previous_usage = 'gnb',
			},
		},
	},
	textobjects = {
		select = {
			enable = true,
			lookahaed = true,
			keymaps = {
				['af'] = {
					query = '@function.outer',
					desc = 'outer part of a function region',
				},
				['if'] = {
					query = '@function.inner',
					desc = 'inner part of a function region',
				},
				['ac'] = {
					query = '@class.outer',
					desc = 'outer part of a class region',
				},
				['ic'] = {
					query = '@class.inner',
					desc = 'inner part of a class region',
				},
			},
			selection_modes = {
				['@paramer.outer'] = 'v',
				['@function.outer'] = 'V',
				['@class.outer'] = '<c-v>',
			},
		},
		lsp_interop = {
			enable = true,
			border = 'none',
			floating_preview_opts = {},
			peek_definition_code = {
				['<leader>tdf'] = '@function.outer',
				['<leader>tdF'] = '@class.outer',
			},
		},
	},
	playground = {
		enable = true,
		disable = {},
		updatetime = 25,
		persist_queries = false,
		keybindings = {
			toggle_query_groups = 'o',
			toggle_hl_groups = 'i',
			toggle_injected_languages = 't',
			toggle_anonymous_nodes = 'a',
			toggle_language_display = 'I',
			focus_language = 'f',
			unfocus_language = 'F',
			update = 'R',
			goto_node = '<cr>',
			show_help = '?',
		},
	},
	query_linter = {
		enable = true,
		use_virtual_text = true,
		lint_events = { 'BufWrite', 'CursorHold' },
	},
}

require('treesitter-context').setup {}

local map = vim.keymap

map.set('n', '<leader>tp', '<cmd>TSPlaygroundToggle<cr>')
map.set('n', '<leader>th', '<cmd>TSHighlightCapturesUnderCursor<cr>')
map.set('n', '<leader>tn', '<cmd>TSNodeUnderCursor<cr>')
