-- plugin manager
require('config.plugins')

-- load plugins
require('config.code')
require('config.mason')
require('config.motion')
require('config.neorg')
require('config.support')
require('config.telescope')
require('config.theme')
require('config.tree')
require('config.treesitter')
