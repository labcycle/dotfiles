require('ibl').setup {
	exclude = {
		buftypes = { 'terminal' },
		filetypes = { 'mason', 'lazy' },
	},
	indent = {
		char = { '|', '¦', '┆', '┊', '⸽' },
	},
}
