local npairs = require('nvim-autopairs')
local rule = require('nvim-autopairs.rule')

npairs.setup {
	check_ts = true,
}
