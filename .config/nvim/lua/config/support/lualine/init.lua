local navic = require('nvim-navic')

require('lualine').setup {
	options = {
		theme = 'tokyonight',
		section_separators = { left = '', right = '' },
		component_separators = { left = '|', right = '|' },
		ignore_focus = {
			'NvimTree',
			'aerial',
			'dap-repl',
			'dapui_breakpoints',
			'dapui_console',
			'dapui_scopes',
			'dapui_stacks',
			'dapui_watches',
			'DiffviewFiles',
			'DiffviewFileHistory',
			'tsplayground',
		},
	},
	sections = {
		lualine_a = {
			{ 'mode', fmt = function (str) return str:sub(1,1) end },
		},
		lualine_c = {
			'filename',
			{ navic.get_location, cond = navic.is_available },
		},
		lualine_x = { 'filetype' },
	},
}
