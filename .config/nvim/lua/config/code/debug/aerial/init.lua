local map = vim.keymap

require('aerial').setup {
	on_attach = function(bufnr)
		map.set('n', '{', '<cmd>AerialPrev<cr>', {buffer = bufnr})
		map.set('n', '}', '<cmd>AerialNext<cr>', {buffer = bufnr})
	end,
	backends = { 'treesitter', 'lsp', 'markdown', 'man' },
	
}

map.set('n', '<leader>l', '<cmd>AerialToggle!<cr>', {desc = 'toggle aerial'})
