require('trouble').setup {}


local map = vim.keymap

map.set('n', '<leader>xx', '<cmd>TroubleToggle<cr>', {silent = true, noremap = true, desc = 'toggle trouble list'})
map.set('n', '<leader>xw', '<cmd>TroubleToggle workspace_diagnostics<cr>', {silent = true, noremap = true, desc = 'toggle trouble workspace diagnostics'})
map.set('n', '<leader>xd', '<cmd>TroubleToggle document_diagnostics<cr>', {silent = true, noremap = true, desc = 'toggle trouble document diagnostics'})
map.set('n', '<leader>xl', '<cmd>TroubleToggle loclist<cr>', {silent = true, noremap = true, desc = 'toggle trouble location list'})
map.set('n', '<leader>xq', '<cmd>TroubleToggle quickfix<cr>', {silent = true, noremap = true, desc = 'toggle trouble quickfix'})
map.set('n', '<leader>xr', '<cmd>TroubleToggle lsp_references<cr>', {silent = true, noremap = true, desc = 'toggle trouble lsp references'})
