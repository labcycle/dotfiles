local dap = require('dap')
local dapui = require('dapui')

dapui.setup {}
dap.listeners.after.event_initaliazed['dapui_config'] = function ()
	dapui.open()
end
dap.listeners.before.event_terminated['dapui_config'] = function ()
	dapui.close()
end
dap.listeners.before.exited['dapui_config'] = function ()
	dapui.close()
end

local map = vim.keymap

map.set({ 'n', 'v' }, '<leader>db', dap.toggle_breakpoint, { desc = 'dap toggle breakpoint' })
map.set({ 'n', 'v' }, '<leader>dz', dap.clear_breakpoints, { desc = 'dap clear breakpoints' })
map.set({ 'n', 'v' }, '<leader>dc', dap.continue, { desc = 'dap continue' })
map.set({ 'n', 'v' }, '<leader>di', dap.step_into, { desc = 'dap step into' })
map.set({ 'n', 'v' }, '<leader>do', dap.step_out, { desc = 'dap step into' })
map.set({ 'n', 'v' }, '<leader>dr', dap.repl.toggle, { desc = 'dap toggle repl' })
map.set({ 'n', 'v' }, '<leader>du', dapui.toggle, { desc = 'dap toggle ui' })
