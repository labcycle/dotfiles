local lspconfig = require('lspconfig')
local api = vim.api

require('neodev').setup {
	library = {
		plugins = { 'nvim-dap-ui' },
		types = true,
	},
}

lspconfig.lua_ls.setup {
	settings = {
		Lua = {
			runtime = {
				version = 'LuaJIT',
			},
			diagnostics = {
				globals = { 'vim' },
			},
			workspace = {
				library = api.nvim_get_runtime_file('', true),
			},
			completion = {
				callSnippet = 'Replace',
			},
			telemetry = {
				enable = true,
			},
		},
	},
}
