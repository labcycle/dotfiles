local capabilities = require('cmp_nvim_lsp').default_capabilities()

local rt = require('rust-tools')
local navic = require('nvim-navic')

local map = vim.keymap

local lsp_attach = function(_, bufnr)
	navic.attach(_, bufnr)
	map.set('n', '<C-space>', rt.hover_actions.hover_actions, { buffer = bufnr })
	map.set('n', '<leader>a', rt.code_action_group.code_action_group, { buffer= bufnr, desc = 'rust-tools action group' })
	rt.inlay_hints.enable()
end

local codelldb_path = vim.fn.stdpath('data') .. '/usr/bin/codelldb'
local liblldb_path = vim.fn.stdpath('data') .. '/usr/lib/liblldb.so'

rt.setup {
	tools = {
		hover_actions = {
			auto_focus = true,
		},
	},
	server = {
		capabilities = capabilities,
		on_attach = lsp_attach,
		settings = {
			['rust-analyzer'] = {
				inlayHints = {
					maxLength = 100,
				},
				cargo = {
					allFeatures = true,
				},
			},
		},
	},
	dap = {
		adapter = require('rust-tools.dap')
			.get_codelldb_adapter(
				codelldb_path,
				liblldb_path
		),
	},
}

