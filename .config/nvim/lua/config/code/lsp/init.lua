require('config.code.lsp.rust')
require('config.code.lsp.lua')

local map = vim.keymap
map.set('n', 'glu', vim.lsp.buf.references, { desc = 'list usages from LSP' })
map.set('n', 'glr', vim.lsp.buf.rename, { desc = 'rename using LSP' })
