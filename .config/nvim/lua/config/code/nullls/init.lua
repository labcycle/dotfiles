local null_ls = require('null-ls')

local sources = {
	null_ls.builtins.code_actions.gitsigns,
}

null_ls.setup {
	sources = sources,
}

local map = vim.keymap

map.set({ 'n', 'v' }, '<leader>ga', vim.lsp.buf.code_action, { desc = 'display code actions' })
