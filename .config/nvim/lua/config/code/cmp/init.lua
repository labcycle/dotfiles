local cmp = require('cmp')
local lspkind = require('lspkind')
local snippy = require('snippy')
local cmp_autopairs = require('nvim-autopairs.completion.cmp')

cmp.setup {
	preselect = cmp.PreselectMode.None,
	view = {
		entries = { name = 'custom', selection_order = 'near_cursor' },
	},
	snippet = {
		expand = function(args)
			snippy.expand_snippet(args.body)
		end,
	},
	window = {
		documentation = cmp.config.window.bordered(),
	},
	mapping = cmp.mapping.preset.insert {
		['<C-b>'] = cmp.mapping.scroll_docs(-4),
		['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<M-Space>'] = cmp.mapping.complete(),
		['<M-e>'] = cmp.mapping.abort(),
		['<M-n>'] = cmp.mapping.select_next_item(),
		['<M-p>'] = cmp.mapping.select_prev_item(),
		['<M-y>'] = cmp.mapping.confirm {
			behavior = cmp.ConfirmBehavior.Insert,
			select = true,
		},
		['<M-N>'] = cmp.mapping.select_next_item(),
		['<M-P>'] = cmp.mapping.select_prev_item(),
		['<M-Y>'] = cmp.mapping.confirm {
			behavior = cmp.ConfirmBehavior.Insert,
			select = true,
		},
	},
	sources = {
		{ name = 'nvim_lsp' },
		{ name = 'nvim_lsp_signature_help' },
		{ name = 'snippy' },
		{ name = 'path' },
		{ name = 'buffer' },
	},
	formatting = {
		format = lspkind.cmp_format {
			mode = 'symbol',
			menu = {
				buffer = '[Buf]',
				nvim_lsp = '[LSP]',
				snippy = '[Snip]',
				path = '[Path]',
			},
		},
	},
	completion = {
		completeopt = 'menu,menuone',
	},
	experimental = {
		ghost_text = true,
	},
}

cmp.setup.cmdline('/', {
	mapping = {
		['<M-e>'] = cmp.mapping.abort(),
		['<M-n>'] = {
			c = function (fallback)
				if cmp.visible() then
					cmp.select_next_item()
				elseif snippy.can_expand_or_advance() then
					snippy.expand_or_advance()
				else
					fallback()
				end
			end,
		},
		['<M-p>'] = {
			c = function (fallback)
				if cmp.visible() then
					cmp.select_prev_item()
				elseif snippy.can_jump(-1) then
					snippy.previous()
				else
					fallback()
				end
			end,
		},
		['<M-y>'] = {
			c = cmp.mapping.confirm {
				behavior = cmp.ConfirmBehavior.Insert,
				select = true,
			},
		},
	},
	sources = {
		{ name = 'buffer' },
	}
})

cmp.setup.cmdline(':', {
	mapping = {
		['<M-e>'] = cmp.mapping.abort(),
		['<M-n>'] = {
			c = function (fallback)
				if cmp.visible() then
					cmp.select_next_item()
				elseif snippy.can_expand_or_advance() then
					snippy.expand_or_advance()
				else
					fallback()
				end
			end,
		},
		['<M-p>'] = {
			c = function (fallback)
				if cmp.visible() then
					cmp.select_prev_item()
				elseif snippy.can_jump(-1) then
					snippy.previous()
				else
					fallback()
				end
			end,
		},
		['<M-y>'] = {
			c = cmp.mapping.confirm {
				behavior = cmp.ConfirmBehavior.Insert,
				select = true,
			},
		},
	},
	sources = {
		{ name = 'cmdline' },
		{ name = 'path' },
	}
})

cmp.event:on {
	'confirm_done',
	cmp_autopairs.on_confirm_done(),
}
