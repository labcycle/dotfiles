require('neorg').setup {
	load = {
		['core.defaults'] = {},
		['core.concealer'] = {},
		['core.dirman'] = {
			config = {
				workspaces = {
					plans = '~/notes/plans',
				},
			},
		},
	},
}
