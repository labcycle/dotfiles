local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system {
		'git',
		'clone',
		'--filter=blob:none',
		'https://github.com/folke/lazy.nvim.git',
		'--branch=stable',
		lazypath,
	}
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup {
	{
		'folke/which-key.nvim',
	},
	{
		'folke/neoconf.nvim',
		cmd = 'Neoconf'
	},
	{
		'folke/neodev.nvim',
	},
	{
		'folke/tokyonight.nvim',
		config = function()
			vim.cmd([[colorscheme tokyonight]])
		end,
	},
	{
		'numToStr/Comment.nvim',
	},
	{
		'lewis6991/gitsigns.nvim',
	},
	{
		'nvim-treesitter/nvim-treesitter',
		dependencies = {
			'nvim-treesitter/nvim-treesitter-textobjects',
			'nvim-treesitter/nvim-treesitter-refactor',
			'nvim-treesitter/nvim-treesitter-context',
			'nvim-treesitter/playground',
		},
		build = ':TSUpdate',
	},
	{
		'nvim-neorg/neorg',
		build = ":Neorg sync-parsers",
		dependencies = { 'nvim-lua/plenary.nvim' },
	},
	{
		'windwp/nvim-autopairs',
	},
	{
		'nvim-lualine/lualine.nvim',
		dependencies = { 'nvim-tree/nvim-web-devicons' },
	},
	{
		'neovim/nvim-lspconfig',
	},
	{
		'lukas-reineke/indent-blankline.nvim',
	},
	{
		'hrsh7th/nvim-cmp',
		dependencies = {
			'hrsh7th/cmp-nvim-lsp',
			'hrsh7th/cmp-buffer',
			'hrsh7th/cmp-path',
			'hrsh7th/cmp-cmdline',
			'dcampos/nvim-snippy',
			'dcampos/cmp-snippy',
			'onsails/lspkind.nvim',
		},
	},
	{
		'nvim-telescope/telescope.nvim',
		tag = '0.1.1',
		dependencies = {
			{ 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
			'nvim-lua/plenary.nvim',
			'nvim-tree/nvim-web-devicons',
		},
	},
	{
		'nvim-tree/nvim-tree.lua',
		dependencies = { 'nvim-tree/nvim-web-devicons' },
	},
	{
		'williamboman/mason.nvim',
		dependencies = { 'williamboman/mason-lspconfig.nvim' },
		build = ":MasonUpdate"
	},
	{
		'SmiteshP/nvim-navic',
		dependencies = { 'neovim/nvim-lspconfig' },
	},
	{
		'mfussenegger/nvim-dap',
	},
	{
		'simrat39/rust-tools.nvim',
		dependencies = {
			'nvim-lua/plenary.nvim',
			'rust-lang/rust.vim',
		},
	},
	{
		'folke/trouble.nvim',
		dependencies = { 'nvim-tree/nvim-web-devicons' },
	},
	{
		'ggandor/leap.nvim',
		dependencies = {
			'tpope/vim-repeat',
			'ggandor/flit.nvim',
		},
	},
	{
		'stevearc/aerial.nvim',
	},
	{
		'kylechui/nvim-surround',
		version = '*',
	},
	{
		'numToStr/Navigator.nvim',
	},
	{
		'rcarriga/nvim-dap-ui',
		version = '*',
		dependencies = { 'mfussenegger/nvim-dap' },
	},
	{
		'jose-elias-alvarez/null-ls.nvim',
		dependencies = { 'nvim-lua/plenary.nvim' },
	},
	{
		'arnamak/stay-centered.nvim',
		config = function ()
			require('stay-centered')
		end,
	},
	{
		'folke/noice.nvim',
		dependencies = {
			'MunifTanjim/nui.nvim',
			'rcarriga/nvim-notify',
		},
	},
}
