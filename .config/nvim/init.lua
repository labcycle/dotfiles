-- init.lua
--

vim.g.mapleader = ','

require('config')


local opt = vim.opt


----COLOURS-------------------------
opt.background = 'dark'
opt.termguicolors = true


----OPTIONS-------------------------
opt.clipboard:append('unnamedplus')
opt.colorcolumn = '100'
opt.foldenable = true
opt.foldexpr = 'nvim_treesitter#foldexpr()'
opt.foldmethod = 'expr'
opt.foldminlines = 5
opt.foldlevel = 1
opt.ignorecase = true
opt.inccommand = 'nosplit'
opt.list = false
opt.number = true
opt.relativenumber = true
opt.shiftwidth = 4
opt.shortmess:append('I')
opt.signcolumn = 'yes:1'
opt.smartcase = true
opt.smartindent = true
opt.splitbelow = true
opt.splitright = true
opt.switchbuf = { 'usetab', 'newtab' }
opt.tabstop = 4


----KEYMAPPINGS-----------------------
------
local map = vim.keymap
------
map.set('n', '<M-[>', '<cmd>resize -5<cr>', { silent = true, desc = 'split resize -5' })
map.set('n', '<M-]>', '<cmd>resize +5<cr>', { silent = true, desc = 'split resize +5' })
map.set('n', '<M-,>', '<cmd>vertical resize -5<cr>', { silent = true, desc = 'split vertical resize -5' })
map.set('n', '<M-.>', '<cmd>vertical resize +5<cr>', { silent = true, desc = 'split vertical resize +5' })
