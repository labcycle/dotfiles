local wezterm = require('wezterm')
local padding = '1px'

return {
	font = wezterm.font('IosevkaTerm NFM'),
	color_scheme = 'tokyonight',
	hide_tab_bar_if_only_one_tab = true,
	use_fancy_tab_bar = false,
	window_padding = {
		left = padding,
		right = padding,
		top = padding,
		bottom = padding,
	},
	colors = {
		tab_bar = {
			active_tab = {
				bg_color = '#1a1b26',
				fg_color = '#c0caf5',
				intensity = 'Bold',
			},
		},
	},
	keys = require('nvim_config'),
}

