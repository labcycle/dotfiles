fish_add_path ~/.cargo/bin

if status is-interactive
    # Commands to run in interactive sessions can go here

	## vim keybindings and cursor config
	set -g fish_key_bindings fish_vi_key_bindings
	set fish_vi_force_cursor
	set fish_cursor_default 	block 		blink
	set fish_cursor_insert 		line
	set fish_cursor_replace_one underscore
	set fish_cursor_visual 		block

	bind -M insert \ey accept-autosuggestion

	## source
	starship init fish | source
	zoxide init fish | source

	## zellij
	set ZELLIJ_AUTO_EXIT true
    eval (zellij setup --generate-auto-start fish | string collect)
end
